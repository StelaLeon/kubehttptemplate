
    kubectl ${action} -f spark-cluster.yaml
        where action E {apply, delete}


	kubectl wait pod --for condition=ready -l app=spark-master --timeout=300s


## Anti patterns:
Do not run multiple services inside a single container. Containers are designed to run only a single process per container(unless the process spawns child processes).
If you run multiple unrelated processes in a
single container, it is your responsibility to keep all those processes running, man-
age their logs, and so on. For example, you’d have to include a mechanism for auto-
matically restarting individual processes if they crash. Also, all those processes would
log to the same standard output, so you’d have a hard time figuring out what pro-
cess logged what.Introducing pods
57
Therefore, you need to run each process in its own container. That’s how Docker
and Kubernetes are meant to be used.
Because containers in a pod run in the same Network namespace, they share the same IP address and port space.


#### Kube-dashboard: an oddisey 
    
    192.168.0.0/16 is a private IP range, meaning you need to be within the cluster's network to access it.
    
    The easiest way to access your service outside the cluster is to run kubectl proxy, which will proxy requests to your localhost port 8001 to the Kubernetes API server. From there, the apiserver can proxy to your service:
    
    http://localhost:8001/api/v1/proxy/namespaces/kube-system/services/kubernetes-dashboard
 kubectl proxy 
to login: https://github.com/kubernetes/dashboard/blob/master/docs/user/access-control/creating-sample-user.md

    stela@Stela:~/IdeaProjects/FileSystemPureFP/infrastructure$ kubectl apply -f dashboard-adminuser.yml
    serviceaccount/admin-user created
    clusterrolebinding.rbac.authorization.k8s.io/admin-user created


    stela@Stela:~/IdeaProjects/FileSystemPureFP/infrastructure$ kubectl -n kubernetes-dashboard describe secret $(kubectl -n kubernetes-dashboard get secret | grep admin-user | awk '{print $1}')


### Commands

    describe a pod    
        $ kubectl get po kubia-zxzij -o yaml
        $ kubectl get pods -n sc
        (-n <namespace>)
        
    describe a node:
        $ kubectl describe node minikube
    
    describe a cluster:
        $ kubectl cluster-info
    
    get nodes:
        $ kubectl get nodes    
        $ kubectl describe svc kubernetes-dashboard
        
        $ kubectl explain (pods)
    create stuff
        $ kubectl create -f <yaml file>
        
    namespaces:
        $ kubectl get namespaces
        $ kubectl get pods --all-namespaces
    
    logs:
        $ kubectl logs -f <pod_name> -n namespace
            
    ###########NOTE Container logs are automatically rotated daily and every time the log file
               reaches 10MB in size. The kubectl logs command only shows the log entries
               from the last rotation.
    
    if your pod has multiple containers, specify container with:
    
    $ kubectl logs <pod name> -c <container_name>

    kubectl expose
    command to create a service to gain access to the pod externally.

    $ kubectl delete all --all
    
### organizing stuff:
    $ kubectl get po --show-labels
    $ kubectl get po -L creation_method,env
    $ kubectl get po -l app=spark-master -n sc
    $ kubectl get po -l '!app' -n sc
    $ kubectl get po -l 'app!=spark-master' -n sc
    NAME                            READY   STATUS             RESTARTS   AGE
    spark-worker-67c6dd96ff-lgrnv   0/1     CrashLoopBackOff   41         14h
            env in (prod,devel)
            env notin (prod,devel)
            app=pc,rel=beta << multiple conditions united by AND
### 
The following command will forward your machine’s local port 8888 to port 8080 of your <pod_name>:
    
    $ kubectl port-forward <pod_name> 8888:8080
    ... Forwarding from 127.0.0.1:8888 -> 8080
    ... Forwarding from [::1]:8888 -> 8080
The port forwarder is running and you can now connect to your pod through the local port.

##Notes 
You cannot ping an IP created by a service, it is a virtual IP hence it only makes sense along with the port. 
If you do not define a pod selector in your service then no endpoints will be created (after all, without a selector, it can’t know which pods to include in the service)
ExternalName services are implemented solely at the DNS level—a simple CNAME
DNS record is created for the service. Therefore, clients connecting to the service will
connect to the external service directly, bypassing the service proxy completely. For
this reason, these types of services don’t even get a cluster IP.

    https://kubernetes.io/docs/reference/kubectl/jsonpath/

minikube doesn\t support loadbalancer type.. it will still be a nodeport
5.3.3
Understanding the peculiarities of external connections 

###Other utility commands:
    $ docker logs <container id> 
    $ minikube addons list
    $ minikube addons enable ingress
    $ kubectl get po --all-namespaces
    

####Debugging
    You may remember you created your first pod with the kubectl run command. In
    chapter 2, I mentioned that this doesn’t create a pod directly, but instead creates a
    ReplicationController, which then creates the pod. As soon as you delete a pod cre-
    ated by the ReplicationController, it immediately creates a new one. To delete the
    pod, you also need to delete the ReplicationController.
    
    