import sbt.{Def, _}
import sbtassembly.AssemblyPlugin.autoImport.assembly
import sbtdocker.DockerPlugin
import sbtdocker.DockerPlugin.autoImport.{Dockerfile, docker, dockerfile}
val dottyVersion = "0.24.0-RC1"
enablePlugins(DockerPlugin)

lazy val akkaHttpVersion = "10.2.1"
lazy val akkaVersion     = "2.6.10"

scalacOptions ++= { if (isDotty.value) Seq("-source:3.0-migration", "-language:implicitConversions") else Nil }

lazy val root = (project in file(".")).settings(
  inThisBuild(
    List(
      organization := "com.example",
      scalaVersion := "2.13.3"
    )
  ),
  resolvers ++= Vector(
    "Sonatype SNAPSHOTS" at "https://oss.sonatype.org/content/repositories/snapshots/"
  ),
  name := "kubeHttpTemplate",
  libraryDependencies ++= Seq(
    ("com.typesafe.akka" %% "akka-http"                % akkaHttpVersion).withDottyCompat(scalaVersion.value),
    ("com.typesafe.akka" %% "akka-http-spray-json"     % akkaHttpVersion).withDottyCompat(scalaVersion.value),
    ("com.typesafe.akka" %% "akka-actor-typed"         % akkaVersion).withDottyCompat(scalaVersion.value),
    ("com.typesafe.akka" %% "akka-stream"              % akkaVersion).withDottyCompat(scalaVersion.value),
    ("ch.qos.logback"     % "logback-classic"          % "1.2.3").withDottyCompat(scalaVersion.value),
    ("com.typesafe.akka" %% "akka-http-testkit"        % akkaHttpVersion % Test).withDottyCompat(scalaVersion.value),
    ("com.typesafe.akka" %% "akka-actor-testkit-typed" % akkaVersion     % Test).withDottyCompat(scalaVersion.value),
    ("org.scalatest"     %% "scalatest"                % "3.0.8"         % Test).withDottyCompat(scalaVersion.value)
  )
)

dockerfile in docker := {
  // The assembly task generates a fat JAR file
  val artifact: File     = assembly.value
  val artifactTargetPath = s"/app/${artifact.name}"
  new Dockerfile {
    from("lampepfl/dotty")
    add(artifact, artifactTargetPath)
    entryPoint("java", "-jar", artifactTargetPath)
    expose(Seq(8080): _*)
  }
}
